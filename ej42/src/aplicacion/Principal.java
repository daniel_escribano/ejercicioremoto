package aplicacion;

//import dominio.*;

public class Principal{
	public static void main(String args[]){
		/*int i = 0; 
		  while(i <= 10){
		  System.out.println(i);
		  i += 2;
		  }*/
		//for(int i = 1; i <= 10; i++) System.out.println(i); //numeros del 0-10 for
		//for(int i = 0; i <=10; i += 2) System.out.println(i); //numeros pares del 0-10 for
		/*int j = 0;
		  while(j > 5){
		  System.out.println(j);
		  }*/
		/*int i = 1;
		  do {
		  System.out.println(i++);
		  }while(i <= -10);*/ //la i será 1 solo una vez (el do dice que al menos se haga una vez ya que el do esta delamte del while
		/*int i = 0;
		  int j = 0;
		  while(i <= 10){
		  j = 0;
		  while(j <= i + 5){
		  System.out.println(i + "," + j);
		  j++;
		  }
		  i++;
		  }*/ //dos numeros en el que la j depende de i hasta 10,15 (i,j)
		/*System.out.println("Ejecutándose primer bucle while...");
		  int i = 1;
		  while(i <= 10) System.out.println(i++);
		  System.out.println("Ejecutándose primer bucle do-while...");
		  i = 1;
		  do System.out.println(i++); while(i <= 10);
		  System.out.println("Ejecutándose segundo bucle while...");
		  i = 1;
		  while(i < 0) System.out.println(i++);
		  System.out.println("Ejecutándose segundo bucle do-while...");
		  i = 1;
		  do System.out.println(i++); while(i < 0);*/
		/*int i = 1;
		  int suma = 0;
		  while(i <= 100){
		  suma = suma + i;
		  i++;
		  }
		  System.out.println(suma);*/ //suma While
		/*int suma = 0;
		for(int i = 0; i <= 100; i++){
			suma = suma + i;
		}
		System.out.println(suma);*/ //suma For
		/*int[] array = new int[10]; //define una estructura para guardar 10 posiciones
		System.out.println(array[0]);
		array[1] = 12;
		array[2] = 7;
		System.out.println(array[1]);
		String[] textos = new String[5];
		System.out.println(textos[0]);
		textos[0] = "Hola";
		System.out.println(textos[0]);
		//System.out.println(array[10]); //hay un error ya que array String (0-5 no 10)
		for(int i = 0 ; i<10; i++){
			System.out.println(array[i]);
		}//for de array
		int i = 0;
		while(i<10){
			System.out.println(array[i]); 
			i++;  //se puede quiatr por el i++ del system para simplificar.
		}*/ //While array
		int[] datos = {0,1,2,3,4,5,6,7,8,9};
		System.out.println(datos[4]);
	}
}
