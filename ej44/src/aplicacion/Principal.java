package aplicacion;

//import dominio.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Principal{
	public static void main(String args[]){
		try{
			FileWriter fw = new FileWriter("fichero.txt");
			fw.write("Contenido del fichero");
			fw.close();
		}catch(IOException e){
			System.out.println("Algo ha fallado al intentar escribir el fichero");
		}
		System.out.println("Se ha escrito el fichero");
		//Scanner del fichero (lee el fichero)
		String nombreFichero = "Fichero1.txt";
		File fichero = new File(nombreFichero);
		try{
			Scanner sc = new Scanner(fichero);
			while(sc.hasNext()){
				System.out.println(sc.next());
			}
			sc.close();
		}catch(FileNotFoundException e){
			System.out.println("Archivo fichero.txt no encontrado");
		}
	}
}
