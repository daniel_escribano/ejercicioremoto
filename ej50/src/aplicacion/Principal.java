package aplicacion;

//import dominio.*;

public class Principal{
	public static void main(String[] args){
		String frase = "No vueles como las aves de corral cuando puedes subir como las águilas";
		//Primer apartado: cadena formada por los 2 primeros caracteres
		String resultado = "La cadena de los dos primeros caracteres es " + frase.charAt(0) + frase.charAt(1);
		System.out.println(resultado);
		//Imprimir el quinto caracter
		System.out.println("El quinto caracter es " + frase.charAt(4));
		//La posición de la palabra puedes
		String[] split = frase.split(" ");
		for (int i=0; i<split.length; i++){
			if(split[i].equals("puedes")){
				System.out.println("La posición de la palabra puedes es la " + i);
				break;
				}
			}
			//Reemplazar aves de corral por gallinas
			System.out.println(frase.replace("aves de corral", "gallinas"));
		}
	}
