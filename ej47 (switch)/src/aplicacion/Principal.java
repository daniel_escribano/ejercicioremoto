package aplicacion;

//import dominio.*;
//import java.io.FileWriter;
//import java.io.IOExceºption;
import java.util.InputMismatchException;
import java.util.Scanner;
//import java.lang.ArithmeticException;
//import java.io.File;
//import java.io.FileNotFoundException;

public class Principal{
	public static void main(String args[]){
		boolean condicion = true;
		Scanner sc = new Scanner(System.in);
		while(condicion){
			System.out.println("Introduzca un numero:");
			try{
				int numero = sc.nextInt();
				condicion = false;
				switch(numero){
					case 1: 
						System.out.println("Lunes");
						break;
					case 2: 
						System.out.println("Martes");
						break;
					case 3:
						System.out.println("Miercoles");
						break;
					case 4:
						System.out.println("Jueves");
						break;
					case 5:
						System.out.println("Viernes");
						break;
					case 6:
						System.out.println("Sabado");
						break;
					case 7:
						System.out.println("Domingo");
						break;
					default: 
						System.out.println("No es un número entre el 1 al 7");
						condicion = true;
				}
			}catch(InputMismatchException e){
				sc.next();
				System.out.println("Has introducido una letra porfavor introduzca un numero");
			}
		}
		sc.close();
	}
}
