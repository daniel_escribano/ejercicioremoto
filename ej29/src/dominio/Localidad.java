package dominio;
/**
 * Esta clase define uan localidad con su nombre y sus habitantes
 * @author Daniel Escribano
*/

public class Localidad
{
	private String nombre;
	private int numeroDeHabitantes;
	/**
	 * Para obtener el nombre de Localidad
	 * @return Nombre de la localidad 
	*/
	public String getNombre()
	{
		return nombre;
	}
	/**
	 * Método para cambiar el nombre de la localidad
	 * @param nombre Nuevo nombre de la localidad
	 */
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	public int getNumeroDeHabitantes()
	{
		return numeroDeHabitantes;
	}
	public void setNumeroDeHabitantes(int
			numeroDeHabitantes)
	{
		this.numeroDeHabitantes =
			numeroDeHabitantes;
	}
	public String toString()
	{
		return nombre + " " + numeroDeHabitantes
			+ " habitantes";
	}
}

